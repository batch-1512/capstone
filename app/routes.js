const { exchangeRates } = require("../src/util.js");

module.exports = (app) => {
  app.get("/", (req, res) => {
    return res.send({ data: {} });
  });

  app.get("/currency", (req, res) => {
    return res.send({
      currency: exchangeRates
    });
  });

  app.post("/currency", (req, res) => {
    if (!req.body.hasOwnProperty("name")) {
      return res.status(400).send({
        error: "Bad Request : missing required parameter NAME",
      });
    }
    if (typeof req.body.name !== "string") {
      return res.status(400).send({
        error: "Bad Request : NAME has to be string",
      });
    }
    if (req.body.name === "") {
      return res.status(400).send({
        error: "Bad Request : NAME cannot be an empty string",
      });
    }
    if (!req.body.hasOwnProperty("ex")) {
      return res.status(400).send({
        error: "Bad Request : missing required parameter EX",
      });
    }
    if (typeof req.body.ex !== "object") {
      return res.status(400).send({
        error: "Bad Request : EX has to be an object",
      });
    }
    if (req.body.ex === {}) {
      return res.status(400).send({
        error: "Bad Request : EX cannot be an empty object",
      });
    }
    if (!req.body.hasOwnProperty("alias")) {
      return res.status(400).send({
        error: "Bad Request : missing required parameter ALIAS",
      });
    }
    if (req.body.alias !== "string") {
      return res.status(400).send({
        error: "Bad Request : ALIAS should be a string",
      });
    }
    if (req.body.alias === "") {
      return res.status(400).send({
        error: "Bad Request : ALIAS cannot be an empty string",
      });
    }    
  });
};

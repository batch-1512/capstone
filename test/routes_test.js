const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");

chai.use(http);

describe("API Test Suite", () => {
  it("Test API Post currency is running", () => {
    chai
      .request("http://localhost:4000")
      .get("/currency")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("Test API Post currency returns 400 if no NAME property", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        usd: 0.2,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post currency returns 400 if NAME is not a string", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        name: true,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post currency returns 400 if NAME is an empty string", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        name: "",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post currency returns 400 if no EX property", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        name: 0.2,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post person returns 400 if EX is not an object", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        ex: true,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post person returns 400 if EX is an empty object", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        ex: {},
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post currency returns 400 if no ALIAS", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({})
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post currency returns 400 if ALIAS is not a string", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        alias: true,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post currency returns 400 if ALIAS is an empty string", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        alias: "",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post currency returns 400 if all fields are complete but there is duplicate ALIAS", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        yen: {
          name: "Japanese Yen",
          ex: {
            peso: 0.47,
            usd: 0.0092,
            won: 10.93,
            yuan: 0.065,
          },
        },
        yen: {
          name: "Philippine Peso",
          ex: {
            usd: 0.02,
            won: 23.39,
            yen: 2.14,
            yuan: 0.14,
          },
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post currency returns 200 if all fields are complete and there are no duplicate ALIASes", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/currency")
      .type("json")
      .send({
        yen: {
          name: "Japanese Yen",
          ex: {
            peso: 0.47,
            usd: 0.0092,
            won: 10.93,
            yuan: 0.065,
          },
        },
        peso: {
          name: "Philippine Peso",
          ex: {
            usd: 0.02,
            won: 23.39,
            yen: 2.14,
            yuan: 0.14,
          },
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });
});
